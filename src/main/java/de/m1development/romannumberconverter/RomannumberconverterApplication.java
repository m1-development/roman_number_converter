package de.m1development.romannumberconverter;

import de.m1development.romannumberconverter.application.SQLiteAuditLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomannumberconverterApplication {

    public static void main(String[] args) {

        SQLiteAuditLogger.createNewAuditLog();

        SpringApplication.run(RomannumberconverterApplication.class, args);
    }
}
