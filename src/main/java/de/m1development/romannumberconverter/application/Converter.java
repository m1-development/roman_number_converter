package de.m1development.romannumberconverter.application;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@Component
public class Converter {

    private static final Map<Integer , String> ROMAN_NUMERAL_PARTS = new HashMap<Integer , String>() {{
        put(1000, "M");
        put(500, "D");
        put(100, "C");
        put(50, "L");
        put(10, "X");
        put(5, "V");
        put(1, "I");
    }};

    private final static int[] POWER_OF_TENS = {1, 10, 100, 1000};

    public String convertDecimal(String input) {
        if (input.isEmpty()) {
            return "";
        }

        Integer value = Integer.valueOf(input);
        return convertInternal(value);
    }

    public String convertBinary(String input) {
        if (input.isEmpty()) {
            return "";
        }

        int value = Integer.parseInt(input, 2);
        return convertInternal(value);
    }

    private String convertInternal(Integer input) {
        String romanLetters = "";
        Integer valueToConvert = input;
        for (int exponent = 3; exponent >= 0; exponent--) {
            int digit = valueToConvert / POWER_OF_TENS[exponent];
            romanLetters = romanLetters.concat(convertDigit(digit, exponent));
            valueToConvert = valueToConvert % POWER_OF_TENS[exponent];
        }
        return romanLetters;
    }

    private String convertDigit(int digit, int exponent) {
        if (exponent == 3) {
            return getDigitForExponentNTimes(digit, POWER_OF_TENS[exponent]);
        }

        switch (digit) {
            case 9: {
                String d1 = ROMAN_NUMERAL_PARTS.get(POWER_OF_TENS[exponent]);
                String d2 = ROMAN_NUMERAL_PARTS.get(POWER_OF_TENS[exponent + 1]);
                return d1 + d2;
            }
            case 8:
            case 7:
            case 6: {
                String d1 = ROMAN_NUMERAL_PARTS.get(5 * POWER_OF_TENS[exponent]);
                String d2 = getDigitForExponentNTimes(digit - 5, POWER_OF_TENS[exponent]);
                return d1 + d2;
            }
            case 5: {
                return ROMAN_NUMERAL_PARTS.get(5 * POWER_OF_TENS[exponent]);
            }
            case 4: {
                String d1 = ROMAN_NUMERAL_PARTS.get(POWER_OF_TENS[exponent]);
                String d2 = ROMAN_NUMERAL_PARTS.get(5 * POWER_OF_TENS[exponent]);
                return d1 + d2;
            }
            case 3:
            case 2:
            case 1: {
                return getDigitForExponentNTimes(digit, POWER_OF_TENS[exponent]);
            }
        }

        return "";
    }

    private String getDigitForExponentNTimes(int digit, int powerOfTen) {
        return String.join("", Collections.nCopies(digit, ROMAN_NUMERAL_PARTS.get(powerOfTen)));
    }

}
