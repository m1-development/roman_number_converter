package de.m1development.romannumberconverter.application;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SQLiteAuditLogger {

    private static String auditLogFile = "auditLog.sqlite";
    private static Connection db = null;

    public static List<String> selectAllFromAuditLog() {
        if (isDBAvailable()) {
            String sql = "SELECT id, conTimestamp, conType, conInput, conOutput FROM auditlog";
            try {
                Statement statement = db.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);

                List<String> auditLog = new ArrayList<>();
                while (resultSet.next()) {
                    String line =
                    resultSet.getString("id") +  "," +
                    resultSet.getString("conTimestamp") +  "," +
                    resultSet.getString("conType") +  "," +
                    resultSet.getString("conInput") +  "," +
                    resultSet.getString("conOutput");
                    auditLog.add(line);
                }

                return auditLog;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return Collections.emptyList();
    }

    public static void insertAuditLogEntry(String conTimestamp, String conType, String conInput, String conOutput) {
        if (isDBAvailable()) {
            String sql = "INSERT INTO auditlog (conTimestamp, conType, conInput, conOutput) VALUES(?,?,?,?);";
            try {
                PreparedStatement statement = db.prepareStatement(sql);
                statement.setString(1, conTimestamp);
                statement.setString(2, conType);
                statement.setString(3, conInput);
                statement.setString(4, conOutput);
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void createNewAuditLog() {
        File dbfile=new File(auditLogFile);
        String url = "jdbc:sqlite:" + dbfile.getAbsolutePath();

        try {
            db = DriverManager.getConnection(url);
            if (db != null) {
                DatabaseMetaData meta = db.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Connected to database on " + url);
                createAuditLogTable();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void createAuditLogTable() {
        if (isDBAvailable()) {
            // SQL statement for creating a new table
            String sql = "CREATE TABLE IF NOT EXISTS auditlog (\n"
                    + "	id integer PRIMARY KEY,\n"
                    + "	conTimestamp text NOT NULL,\n"
                    + "	conType text NOT NULL,\n"
                    + "	conInput text,\n"
                    + "	conOutput text\n"
                    + ");";
            try {
                Statement statement = db.createStatement();
                statement.execute(sql);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static boolean isDBAvailable() {
        if (db == null) {
            System.out.println("no DB connection available");
            return false;
        }

        return true;
    }
}
