package de.m1development.romannumberconverter.controller;

import de.m1development.romannumberconverter.application.Converter;
import de.m1development.romannumberconverter.application.SQLiteAuditLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class RESTController {

    @Autowired
    Converter converter;

    @RequestMapping("/")
    public String description() {
        return "This is a roman number converter - usage '/convertDecimal/{decimal number}' or '/convertBinary/{binary number}'";
    }

    @RequestMapping(value = "convertDecimal/{input}")
    public String convertDecimal(@PathVariable("input") String input) {
        String converted = converter.convertDecimal(input);
        SQLiteAuditLogger.insertAuditLogEntry(LocalDateTime.now().toString(), "decimal", input, converted);
        return converted;
    }

    @RequestMapping(value = "convertBinary/{input}")
    public String convertBinary(@PathVariable("input") String input) {
        String converted = converter.convertBinary(input);
        SQLiteAuditLogger.insertAuditLogEntry(LocalDateTime.now().toString(), "binary", input, converted);
        return converted;
    }

    @RequestMapping(value = "auditLog")
    public List<String> auditLog() {
        return SQLiteAuditLogger.selectAllFromAuditLog();
    }
}
