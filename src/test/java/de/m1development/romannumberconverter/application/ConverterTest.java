package de.m1development.romannumberconverter.application;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConverterTest {

    static private final Converter sut = new Converter();

    @ParameterizedTest(name = "Test {index} - input:  {0}, expected: {1}")
    @MethodSource("provideDecimalsForConversion")
    public void convertDecimalTestParameterized(String input, String expected) {
        // given

        // when
        String result = sut.convertDecimal(input);

        //then
        assertNotNull(sut);
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideDecimalsForConversion() {
        return Stream.of(
                Arguments.of("",""),
                Arguments.of("1","I"),
                Arguments.of("2","II"),
                Arguments.of("3","III"),
                Arguments.of("4","IV"),
                Arguments.of("5","V"),
                Arguments.of("6","VI"),
                Arguments.of("7","VII"),
                Arguments.of("8","VIII"),
                Arguments.of("9","IX"),
                Arguments.of("10","X"),
                Arguments.of("19","XIX"),
                Arguments.of("40","XL"),
                Arguments.of("49","XLIX"),
                Arguments.of("50","L"),
                Arguments.of("90","XC"),
                Arguments.of("99","XCIX"),
                Arguments.of("100","C"),
                Arguments.of("400","CD"),
                Arguments.of("500","D"),
                Arguments.of("555","DLV"),
                Arguments.of("900","CM"),
                Arguments.of("1000","M"),
                Arguments.of("2000","MM"),
                Arguments.of("3000","MMM"),
                Arguments.of("4000","MMMM"),
                Arguments.of("5000","MMMMM"),
                Arguments.of("6000","MMMMMM"),
                Arguments.of("7000","MMMMMMM"),
                Arguments.of("8000","MMMMMMMM"),
                Arguments.of("9000","MMMMMMMMM"),
                Arguments.of("1980","MCMLXXX")
        );
    }

    @ParameterizedTest(name = "Test {index} - input:  {0}, expected: {1}")
    @MethodSource("provideBinarisForConversion")
    public void convertBinaryTestParameterized(String input, String expected) {
        // given

        // when
        String result = sut.convertBinary(input);

        //then
        assertNotNull(sut);
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBinarisForConversion() {
        return Stream.of(
                Arguments.of("",""),
                Arguments.of("0001","I"),
                Arguments.of("0010","II"),
                Arguments.of("0100","IV"),
                Arguments.of("1000","VIII"),
                Arguments.of("1010","X")
        );
    }

}